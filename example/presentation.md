title: hexlet.io
author:
  name: Hexlet
  twitter: hexletHQ
  url: https://hexlet.io
style: assets/style.css

--

# hexlet.io
## JS HTTP Server: Example

--

### Необходимые знания

**Операционные системы**

Процессы, Сокеты

**Сети**

TCP/IP

--

### Network

<img width="100%" src="assets/tcp-ip.jpg">

--

### Operation System

* Единица исполнения в ОС это процесс
* Программа это один или несколько процессов
* Каждый процесс имеет идентификатор PID

```sh
$ ps xf

PID TTY      STAT   TIME COMMAND
29 pts/1    S      0:00 bash --norc
30 pts/1    S+     0:00  \_ top
22 pts/0    S      0:00 bash --norc
32 pts/0    R+     0:00  \_ ps xf
```

--

### TCP

> Коммуникация между компьютерами это общение между конкретными процессами
> операционной системы

--

### TCP

```sh
$ lsof -i :4000

COMMAND   PID     USER TYPE NODE NAME
node    40726 mokevnin IPv6 TCP  *:terabase (LISTEN)
```

--

### HTTP Server

```javascript
// server.js
import http from 'http';

const port = 4000;

http.createServer((request, response) => {
  // content-length
  response.write('hello, world!');
  response.end();
}).listen(port, () => {
  console.log('Server has been started');
});
```

```sh
# bash
$ babel-node server.js # blocking
```
--

### Request

```sh
$ telnet localhost 4000
Trying ::1...
Connected to localhost.
Escape character is '^]'.
GET / HTTP/1.1
host: hexlet.io
connection: close

HTTP/1.1 200 OK
Date: Wed, 28 Sep 2016 20:45:20 GMT
Connection: close
Content-Length: 13

hello, world!Connection closed by foreign host.
```

--

### Особенности

* Запуск сервера блокирует терминал
* Код находится в памяти
* Изменение кода не влияет на работу запущенного сервера
* Повторный запуск приведет к ошибке `Error: listen EADDRINUSE :::4000`
