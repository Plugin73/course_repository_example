Разрабатывая сайты и, в особенности, веб-сервисы, приходится постоянно выполнять
http запросы, проверяя не только тело ответа, но и заголовки.

Самый надежный и деревянный способ делать запросы, без которого иногда не
обойтись - это `telnet`. Мы его подробно разбирали в курсе `http`.
Но пользоваться им в повседневной практике не удобно, вбивать запросы долго и
муторно. К счастью есть способ легче это `cURL`.

```
cURL — кроссплатформенная служебная программа командной строки,
позволяющая взаимодействовать с множеством различных серверов
по множеству различных протоколов с синтаксисом URL.
```

Самый простой способ сделать запрос с помощью `curl` выглядит так:

```sh
$ curl hexlet.io
<html>
<head><title>301 Moved Permanently</title></head>
<body bgcolor="white">
<center><h1>301 Moved Permanently</h1></center>
<hr><center>nginx/1.9.14</center>
</body>
</html>
```

По умолчанию запрос делается по протоколу http, а в `stdout` выводится
тело ответа.

Часто бывает нужно увидеть только заголовки ответа, без самого тела.
Для этого достаточно добавить флаг `--head`. Этот флаг меняет тип запроса
с `GET` на `HEAD`.

```sh
$ curl --head hexlet.io
HTTP/1.1 301 Moved Permanently
Content-Length: 185
Content-Type: text/html
Date: Thu, 06 Oct 2016 11:49:24 GMT
Location: https://hexlet.io/
Server: nginx/1.9.14
Connection: keep-alive
```

При необходимости увидеть и запрос и ответ целиком, можно воспользоваться
флагом `-v` который включает `verbose` режим.

```sh
$ curl --v hexlet.io
curl: option --v: is ambiguous
curl: try 'curl --help' or 'curl --manual' for more information
 (master) js-http-server$ curl -v hexlet.io
* Rebuilt URL to: hexlet.io/
*   Trying 52.49.86.185...
* Connected to hexlet.io (52.49.86.185) port 80 (#0)
> GET / HTTP/1.1
> Host: hexlet.io
> User-Agent: curl/7.43.0
> Accept: */*
>
< HTTP/1.1 301 Moved Permanently
< Content-Type: text/html
< Date: Thu, 06 Oct 2016 11:52:07 GMT
< Location: https://hexlet.io/
< Server: nginx/1.9.14
< Content-Length: 185
< Connection: keep-alive
<
<html>
<head><title>301 Moved Permanently</title></head>
<body bgcolor="white">
<center><h1>301 Moved Permanently</h1></center>
<hr><center>nginx/1.9.14</center>
</body>
</html>
* Connection #0 to host hexlet.io left intact
```

Как видно этот вывод содержит в себе вообще все.
